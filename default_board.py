from chess_logic import Pawn, Rook, Knight, Bishop, Queen, King, notation_to_point


def load_board():
    pieces = {
        "a2": (Pawn, "white"),
        "b2": (Pawn, "white"),
        "c2": (Pawn, "white"),
        "d2": (Pawn, "white"),
        "e2": (Pawn, "white"),
        "f2": (Pawn, "white"),
        "g2": (Pawn, "white"),
        "h2": (Pawn, "white"),
        "a1": (Rook, "white"),
        "h1": (Rook, "white"),
        "b1": (Knight, "white"),
        "g1": (Knight, "white"),
        "c1": (Bishop, "white"),
        "f1": (Bishop, "white"),
        "d1": (Queen, "white"),
        "e1": (King, "white"),
        "a7": (Pawn, "black"),
        "b7": (Pawn, "black"),
        "c7": (Pawn, "black"),
        "d7": (Pawn, "black"),
        "e7": (Pawn, "black"),
        "f7": (Pawn, "black"),
        "g7": (Pawn, "black"),
        "h7": (Pawn, "black"),
        "a8": (Rook, "black"),
        "h8": (Rook, "black"),
        "b8": (Knight, "black"),
        "g8": (Knight, "black"),
        "c8": (Bishop, "black"),
        "f8": (Bishop, "black"),
        "d8": (Queen, "black"),
        "e8": (King, "black")
    }

    return [piece_class(notation_to_point(key), color) for key, (piece_class, color) in pieces.items()]


def string_to_piece(name: str):
    mapping = {
        "pawn": Pawn,
        "rook": Rook,
        "knight": Knight,
        "bishop": Bishop,
        "king": King,
        "queen": Queen
    }
    try:
        return mapping[name]
    except KeyError:
        raise KeyError("Not able to interpret piece name", name)
