import draw_chess
from draw_chess import Point, DrawersSettings


class MouseTracker(DrawersSettings):
    def __init__(self):
        DrawersSettings.__init__(self)
        self.x = 0
        self.y = 0
        self.state_changed = False

    def get_tile_clicked(self):
        i = self.x // int(draw_chess.window.width / draw_chess.num_tiles)
        j = self.y // int(draw_chess.window.height / draw_chess.num_tiles)

        return Point(i, j)
