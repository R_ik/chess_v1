from draw_chess import DrawableImage
from abc import ABC, abstractmethod
import itertools
import copy
from point import Point


class Color:
    def __init__(self, color: str):
        if color.lower() == "white":
            self.is_white = True
        else:
            self.is_white = False
        self.is_black = not self.is_white


def find_piece_in_list(considered_location: Point, pieces_list: list):
    return list(filter(lambda p: p.position == considered_location, pieces_list))


class ChessPiece(DrawableImage, Color, ABC):
    def __init__(self, point: Point, color: str, white_image: str, black_image: str):
        Color.__init__(self, color)
        self.possible_moves = []
        self.has_moved = False
        image_path = white_image
        if self.is_black:
            image_path = black_image
        DrawableImage.__init__(self, image_path, point)

    def update(self, pieces):
        self.find_possible_moves(pieces)

    def consider_point_list(self, possible_moves, pieces_list):
        allowed_moves = []
        for move in possible_moves:
            new_position = self.position + move
            if new_position.in_bounds():
                possible_occupant = find_piece_in_list(new_position, pieces_list)
                if possible_occupant:
                    occupant = possible_occupant[0]
                    if occupant.is_white == self.is_white:
                        continue
                allowed_moves.append(new_position)
        return allowed_moves

    def move_to(self, move_to_position: Point):
        self.has_moved = True
        if move_to_position in self.possible_moves:
            self.position = move_to_position
        else:
            raise ValueError

    def traverse_directions(self, step_directions: list, pieces: list, max_steps=None, capture_allowed=True):
        allowed_moves = []
        for step_direction in step_directions:
            allowed_moves_direction = []
            considered_location = self.position + step_direction
            while considered_location.in_bounds():
                possible_occupant = find_piece_in_list(considered_location, pieces)
                if len(possible_occupant) > 0:
                    occupant = possible_occupant[0]
                    if occupant.is_white != self.is_white and capture_allowed:
                        allowed_moves_direction.append(considered_location)
                    break
                allowed_moves_direction.append(considered_location)
                considered_location += step_direction
                if max_steps is not None and len(allowed_moves_direction) >= max_steps:
                    break
            allowed_moves.extend(allowed_moves_direction)
        return allowed_moves

    @abstractmethod
    def find_possible_moves(self, pieces):
        return []

    def __repr__(self):
        return f"{self.__class__}\tpos:{self.position}, \tis_white: {self.is_white}\thas_moved: {self.has_moved}"


def place_in_alphabet(c: chr):
    return ord(c.lower()) - ord('a')


def notation_to_point(notation: str):
    x = 7 - place_in_alphabet(notation[0])
    y = int(notation[1]) - 1
    return Point(x, y)


class Pawn(ChessPiece):
    def __init__(self, point: Point, color: str):
        white_image = "WhitePawn.png"
        black_image = "BlackPawn.png"
        ChessPiece.__init__(self, point, color, white_image, black_image)
        self.moves_down = self.is_black
        self.normal_direction = [Point(0, 1)]
        self.max_steps = 1
        self.capture_moves = [Point(1,1), Point(-1,1)]

    def consider_capture_list(self, possible_moves: list, pieces: list):
        possible_moves = self.consider_point_list(possible_moves, pieces)
        return [p for p in possible_moves if find_piece_in_list(p, pieces)]

    def find_possible_moves(self, pieces):
        forward_direction = copy.deepcopy(self.normal_direction)
        max_forward_steps = self.max_steps
        capture_moves = copy.deepcopy(self.capture_moves)
        if not self.has_moved:
            max_forward_steps += 1
        if self.moves_down:
            forward_direction = [p.mirror_y() for p in forward_direction]
            capture_moves = [p.mirror_y() for p in capture_moves]
        valid_moves = self.traverse_directions(forward_direction, pieces, capture_allowed=False, max_steps=max_forward_steps)
        valid_capture_moves = self.consider_capture_list(capture_moves, pieces)
        valid_moves.extend(valid_capture_moves)
        self.possible_moves = valid_moves

    def is_double_move(self, move: Point):
        return abs(move.y) == 2

    def move_to(self, move_to_position: Point):
        self.has_moved = True
        move = move_to_position - self.position
        if self.is_double_move(move):
            # en_passant_fields_to_check
            pass
            #TODO Add something to search for other pieces that could capture it en passant
        if move_to_position in self.possible_moves:
            self.position = move_to_position
        else:
            raise ValueError


class Rook(ChessPiece):
    def __init__(self, point: Point, color: str):
        white_image = "WhiteRook.png"
        black_image = "BlackRook.png"
        ChessPiece.__init__(self, point, color, white_image, black_image)

    def find_possible_moves(self, pieces):
        directions = [Point(0, 1), Point(1, 0), Point(-1, 0), Point(0, -1)]
        possible_points = self.traverse_directions(directions, pieces)
        self.possible_moves = possible_points


class Knight(ChessPiece):
    def __init__(self, point: Point, color: str):
        white_image = "WhiteKnight.png"
        black_image = "BlackKnight.png"
        ChessPiece.__init__(self, point, color, white_image, black_image)

    def find_possible_moves(self, pieces):
        possible_moves = [Point(*loc) for loc in [(1, 2), (2, 1),(1, -2), (-2, 1),(-1, 2), (2, -1),(-1, -2), (-2, -1)]]
        self.possible_moves = self.consider_point_list(possible_moves, pieces)


class Bishop(ChessPiece):
    def __init__(self, point: Point, color: str):
        white_image = "WhiteBishop.png"
        black_image = "BlackBishop.png"
        ChessPiece.__init__(self, point, color, white_image, black_image)

    def find_possible_moves(self, pieces):
        directions = [Point(*loc) for loc in itertools.product((1,-1),repeat=2)]
        possible_points = self.traverse_directions(directions, pieces)
        self.possible_moves = possible_points


class Queen(ChessPiece):
    def __init__(self, point: Point, color: str):
        white_image = "WhiteQueen.png"
        black_image = "BlackQueen.png"
        ChessPiece.__init__(self, point, color, white_image, black_image)

    def find_possible_moves(self, pieces):
        directions = [Point(*loc) for loc in [(0,1),(1,0),(-1,0),(0,-1),(1,1),(-1,-1),(-1,1),(1,-1)]]
        possible_points = self.traverse_directions(directions, pieces)
        self.possible_moves = possible_points


class King(ChessPiece):
    def __init__(self, point: Point, color: str):
        white_image = "WhiteKing.png"
        black_image = "BlackKing.png"
        ChessPiece.__init__(self, point, color, white_image, black_image)
        self.is_in_check = False
        #TODO When can we best check if the King is in check? We need to check all the moves of all the pieces for that

    def can_castle(self, direction: str):
        if self.has_moved:
            return False

    def find_possible_moves(self, pieces):
        #TODO add support for Castling
        # Neither the king nor the rook has previously moved during the game.
        # There are no pieces between the king and the rook.
        # The king cannot be in check, nor can the king pass through any square that is under attack by an enemy piece, or move to a square that would result in check. (Note that castling is permitted if the rook is under attack, or if the rook crosses an attacked square.)
        if not self.can_castle("left"):
            # find
            pass
        possible_moves = [Point(*loc) for loc in [(0,1),(1,0),(-1,0),(0,-1),(1,1),(-1,-1),(-1,1),(1,-1)]]
        self.possible_moves = self.consider_point_list(possible_moves, pieces)
