from point import Point
import draw_chess
import default_board

class GameBoard:
    def __init__(self):
        self.selected_tile = Point(0, 0)
        self.pieces = default_board.load_board()
        self.possible_moves = []
        self.player_turn = True
        self.current_piece_index = None

    def draw(self):
        draw_chess.draw_board(self.selected_tile)

    def update(self):
        for piece in self.pieces:
            piece.update(self.pieces)
        self.get_current_piece_index()

    def get_current_piece_index(self):
        self.current_piece_index = None
        for piece in self.pieces:
            if piece.position == self.selected_tile:
                self.current_piece_index = self.pieces.index(piece)

    def find_piece_index_for_tile(self):
        try:
            return [idx for idx, piece in enumerate(self.pieces) if piece.position == self.selected_tile][0]
        except IndexError:
            return None

    def next_turn(self):
        self.player_turn = not self.player_turn
