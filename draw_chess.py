from pyglet_helper import *
from point import Point

BLACK = (0, 0, 0, 255)
WHITE = (255, 255, 255, 255)
GREY = (122, 122, 122, 255)
GREEN = (0, 255, 0, 255)
RED = (255, 0, 0, 255)
selected_tile_color = RED
checker_color = GREY
num_tiles = 8


class DrawersSettings:
    def __init__(self):
        self.view = "white"


def init():
    global screen_size, num_tiles, window
    window = get_window()
    set_resource_path()


def to_coordinates(p: Point):
    return p.x * window.width / num_tiles, p.y * window.height / num_tiles


def to_centred_coordinates(p: Point):
    return (p.x * window.width / num_tiles + window.width / num_tiles // 2,
            p.y * window.height / num_tiles + window.height / num_tiles // 2)


def draw_board(selected_tile):
    set_background(WHITE)
    window.clear()

    def draw_tile(i, j, color):
        draw_rect(i * window.width / num_tiles, j * window.height / num_tiles, window.width / num_tiles, window.height / num_tiles, color)
    for i in range(num_tiles):
        for j in range(num_tiles):
            if i == selected_tile.x and j == selected_tile.y:
                draw_tile(i, j, selected_tile_color)
            else:
                if (i + j) % 2:
                    draw_tile(i, j, checker_color)


def draw_dot(position: Point):
    dot_width = window.width / num_tiles // 6
    dot_height = window.height / num_tiles // 6
    dot_x, dot_y = to_centred_coordinates(position)
    dot_x -= dot_width // 2
    dot_y -= dot_height // 2
    draw_rect(dot_x, dot_y, dot_width, dot_height, RED)


class DrawableImage(DrawersSettings):
    def __init__(self, image_path: str, point: Point):
        self.image = pyglet.resource.image(image_path)
        self.sprite = pyglet.sprite.Sprite(img=self.image)
        self.position = point
        DrawersSettings.__init__(self)

    def draw(self):
        new_x, new_y = to_coordinates(self.position)
        self.sprite.update(x=new_x, y=new_y, scale_x=window.width / num_tiles / self.image.width,
                      scale_y=window.height / num_tiles / self.image.height)
        self.sprite.draw()


def draw_possible_moves(possible_moves: list):
    for point in possible_moves:
        draw_dot(point)
