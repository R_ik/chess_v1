from dataclasses import dataclass


@dataclass
class Point:
    x: int
    y: int

    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Point(self.x - other.x, self.y - other.y)

    def coordinates(self):
        return self.x, self.y

    def mirror_y(self):
        self.y = -self.y
        return self

    def in_bounds(self):
        return 0 <= self.x < 8 and 0 <= self.y < 8

    def invert(self):
        self.y = -self.y
