import pyglet
from game_board import GameBoard
from mouse_tracker import MouseTracker
from pyglet_helper import BUTTON_MOUSE_DOWN

import draw_chess
draw_chess.init()

mouse_tracker = MouseTracker()
board = GameBoard()
board.update()

@draw_chess.window.event
def on_mouse_press(x, y, button, _):
    global board
    if button == BUTTON_MOUSE_DOWN:
        mouse_tracker.x = x
        mouse_tracker.y = y
    clicked_tile = mouse_tracker.get_tile_clicked()

    if board.selected_tile != clicked_tile:
        board.selected_tile = clicked_tile

        # check if we can make a move
        if board.selected_tile in board.possible_moves:
            captured_piece_index = board.find_piece_index_for_tile()
            board.pieces[board.current_piece_index].move_to(board.selected_tile)
            if captured_piece_index is not None:
                board.pieces.pop(captured_piece_index)
            board.next_turn()

        # update new state of the board
        board.update()

        # draw player options if a piece is selected
        board.possible_moves = []
        if board.current_piece_index is not None:
            if board.pieces[board.current_piece_index].is_white == board.player_turn:
                board.possible_moves = board.pieces[board.current_piece_index].possible_moves


@draw_chess.window.event
def on_draw():
    global board

    board.draw()
    for piece in board.pieces:
        piece.draw()
    draw_chess.draw_possible_moves(board.possible_moves)


pyglet.app.run()
