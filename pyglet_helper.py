import pyglet

BUTTON_MOUSE_DOWN = 1


def draw_rect(x, y, width, height, color):
    width = int(round(width))
    height = int(round(height))
    image_pattern = pyglet.image.SolidColorImagePattern(color=color)
    image = image_pattern.create_image(width, height)
    image.blit(x, y)


def set_background(color):
    pyglet.gl.glClearColor(1, 1, 1, 1)


def get_window():
    return pyglet.window.Window(width=600, height=600, resizable=True)


def set_resource_path():
    pyglet.resource.path = ['./assets']
    pyglet.resource.reindex()
